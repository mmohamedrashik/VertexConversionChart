package com.vijaysekhar.vertexchart;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
SeekBar spectacle_power,vertex_distance;
ImageView sp_minus,sp_plus,vd_minus,vd_plus;
TextView sp_sign,sp_data,vd_sign,vd_data,cp_sign,cp_value;
FloatingActionButton reset;
    int step = 1;
    int max = 140;
    int min = -20;
    int v_max = 17;
    int v_min = 7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sp_data  = (TextView)findViewById(R.id.specpower_result);
        sp_minus = (ImageView)findViewById(R.id.sp_minus);
        vd_minus = (ImageView)findViewById(R.id.v_minus);
        vd_plus  = (ImageView)findViewById(R.id.v_plus);
        vd_data = (TextView) findViewById(R.id.v_distance);
        cp_value = (TextView)findViewById(R.id.c_power);
        spectacle_power = (SeekBar)findViewById(R.id.spec_seek);
        vertex_distance = (SeekBar)findViewById(R.id.v_seekbar);
        sp_plus = (ImageView)findViewById(R.id.sp_plus);
        reset = (FloatingActionButton)findViewById(R.id.reset);



        spectacle_power.setMax((max - min) / step);
        vertex_distance.setMax((v_max - v_min) / step);
        spectacle_power.setProgress(80);
        vertex_distance.setProgress(5);

        spectacle_power.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                double d = 0.25;
                double value = min + (progress * d);
               // Log.w("SE", "" + progress);
                if(progress>80)
                {
                    sp_data.setText("+"+value);
                }else
                {
                    sp_data.setText(value + "");
                }

                Calculate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });
        sp_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spectacle_power.incrementProgressBy(1);
            }
        });
        sp_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spectacle_power.incrementProgressBy(-1);
            }
        });
        vertex_distance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double value = v_min + (progress * step);
                vd_data.setText(value + "");
               // Log.d("--",""+progress);
                Calculate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        vd_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vertex_distance.incrementProgressBy(1);
            }
        });
        vd_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vertex_distance.incrementProgressBy(-1);
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spectacle_power.setProgress(80);
                vertex_distance.setProgress(5);
            }
        });

    }
    public void Calculate()
    {
        double r;
        int spec_data = spectacle_power.getProgress();
        double d = 0.25;
        double s_value = min + (spec_data * d);
        int vertex_data = vertex_distance.getProgress();
        double v_value = v_min + (vertex_data * step);

        //cp_value.setText(s_value+"  "+v_value);
        double vertex = v_value/1000;
        if ( (s_value >= 4.00) || (s_value <= -4.00) ) {
            r = s_value/(1-s_value*vertex);
        } else {
            r = s_value;
        }
        Float float1 = (float)r;
        if(getRoundedValue(float1)>0)
        {
            cp_value.setText("+"+getRoundedValue(float1).toString());
        }else
        {
            cp_value.setText(getRoundedValue(float1).toString());
        }


    }
    private Float getRoundedValue(float p) {
        int nega;
        int i;
        Float result;
        float rem;

        if (p < 0) {
            nega = 1;
            p = -p;
        } else {
            nega = 0;
        }
        i = (int)p;
        result = 0.0F;
        rem = p - i;

        if ( rem < .12500 ) {
            result = (float)i;
        } else if ( (rem >= .125) && (rem <.375) ) {
            result = (float)i + 0.25F;
        } else if ( (rem >= .375) && (rem < .625) ) {
            result = (float)i + 0.50F;
        } else if ( (rem >= .625) && (rem < .875) ) {
            result = (float)i + 0.75F;
        } else if ( rem >= .875 ) {
            result = (float)i + 1.00F;
        }
        if ( nega == 1 ) {
            return -result;
        } else {
            return result;
        }


    }



}
